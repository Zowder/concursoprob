const data = []
var fs = require('fs');

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

const generateMockData = (min, max) => {
  for (i = min; i <= max; i++ ) {
    const row = {
      id: i,
      candidato: `Candidato ${i}`,
      conteudoespecifico: getRandomInt(0, 41),
      linguaportuguesa: getRandomInt(0, 21),
      legislacao: getRandomInt(0, 11),
      informatica: getRandomInt(0, 16)
    }
    data.push(row)
  }
}

generateMockData(1, 1000)
//const disc = ['conteudoespecifico', 'linguaportuguesa', 'legislacao', 'informatica']

const getData = (arr, disc) => {
  return arr.map((obj) => {
    return obj[disc]
  })
}

const calcmedian = (data) => {
  const sum = data.reduce((sum, value) => {
    return sum + value
  }, 0);
  const avg = sum / data.length
  return avg
}

const std = (data) => {
  const avg = calcmedian(data);
  const dif = data.map((value) => {
    const diff = value - avg
    const sqrdiff = diff * diff;
    return sqrdiff
  })
  const avgdif = calcmedian(dif)
  const stdev = Math.sqrt(avgdif)
  return stdev
}

const datace = getData(data, 'conteudoespecifico')
const datalp = getData(data, 'linguaportuguesa')
const dataleg = getData(data, 'legislacao')
const datainfo = getData(data, 'informatica')

const desvioce = std(datace) // desvio padrão da disciplina de conteudoespecifico
const desviolp = std(datalp) // desvio padrão da disciplina de lingua portuguesa
const desvioleg = std(dataleg) // desvio padrão da disciplina de legislacao
const desvioinfo = std(datainfo) // desvio padrão da disciplina de informática

const avgce = calcmedian(datace)
const avglp = calcmedian(datalp)
const avgleg = calcmedian(dataleg)
const avginfo = calcmedian(datainfo)

const np = []

data.map((obj) => {
  const ce = 'conteudoespecifico'
  const lp = 'linguaportuguesa'
  const leg = 'legislacao'
  const info = 'informatica'
  const padraoce = (((obj.conteudoespecifico - avgce) / desvioce) * 100) + 500
  const padraolp = (((obj.linguaportuguesa - avglp) / desviolp) * 100) + 500
  const padraoleg = (((obj.legislacao - avgleg) / desvioleg) * 100) + 500
  const padraoinfo = (((obj.informatica - avginfo) / desvioinfo) * 100) + 500
  const row = {
    id: obj.id,
    candidato: obj.candidato,
    padraoce: padraoce,
    padraolp: padraolp,
    padraoleg: padraoleg,
    padraoinfo: padraoinfo
  }
  np.push(row)
})

const medias = []

np.map((obj) => {
  const pesos = 3.0 + 2.5 + 1.5 + 1.0
  const media = obj.padraoce * 3
  const media1 = obj.padraolp * 2.5
  const media2 = obj.padraoleg * 1.5
  const media3 = obj.padraoinfo * 1
  const mediageral = (media1 + media2 + media3 + media) / pesos
  const row = {
    id: obj.id,
    candidato: obj.candidato,
    media: mediageral
  }
  medias.push(row)
})

//medias.map((obj) => { console.log(obj)})

const compare = (a,b) => {
  if (a.media < b.media)
    return 1;
  if (a.media > b.media)
    return -1;
  return 0;
}

const sort = medias.sort(compare)
const d = []
sort.map((obj, i) => {
  np.filter((c) => {
    if(c.id === obj.id) {
      const row = {
        candidato: obj.candidato,
        ContEsp: c.padraoce,
        LingPort: c.padraolp,
        Leg: c.padraoleg,
        Info: c.padraoinfo,
        MediaPontos: obj.media,
        Colocacao: `${i+1}º`
      }
      d.push(row)
    }
  })
})

const content = JSON.stringify(d);
fs.writeFile("./pontosfinal.json", content, 'utf8', function (err) {
    if (err) {
        return console.log(err);
    }

    console.log("The file was saved!");
}); 
